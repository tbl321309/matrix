**@cathub** report this [AdWare][catinfo] related domain to be added into the [MyPDNS RPZ Firewall][mpdrf]

```
domain_name_here
```

- [X] Wildcarded
- [ ] Individual domain blocking

## RPZ (Response Policy Zone) Rules

```css
domain_name_here   CNAME . ; AdWare, Strict.Adult
*.domain_name_here   CNAME . ; AdWare, Strict.Adult
```

### Additional requirements for

#### [hosts] and [Pi-hole]

```css
adservice.domain_name_here
```

```css
www.domain_name_here
```

#### Adblocker
<details><summary>Click to expand</summary>

```css
N/A
```

</details>

## Screenshots

<details><summary><b><i>NSFW</i></b> Screenshot</summary>



</details>

## Comments
- Reported URL
```
https://adservice.domain_name_here
```
- by Github `ShadowWhisperer`

- IP:`172.217.16.194` [US] AS15169 GOOGLE
- Redirected: `domain_name_here` --> `www.domain_name_here` (`https://www.domain_name_here/?gws_rd=ssl`)

## My Privacy DNS issues
- `google.com` `#1078012`


## External sources
- `https://raw.githubusercontent.com/ShadowWhisperer/BlockLists/master/RAW/Ads`


### All Submissions:
- [X] Have you followed the guidelines in our Contributing document?
- [X] Have you checked to ensure there aren't other open MR or Issues for the same update/change?
- [X] Have you added an explanation of what your submission do and why you'd like us to include them??
- [X] Added screenshot for prove of False Negative

### Testing face
- [X] Checked the internet for verification?
- [X] Have you successfully ran tests with your changes locally?

### Todo
- [X] RPZ Server
- [X] Added to Source file

#### Logger output

<details><summary>3rd party Domains</summary>

```css
www.google.com
```

</details>

[catinfo]: https://mypdns.org/MypDNS/support/-/wikis/Categories/Adware
[FN]: https://mypdns.org/MypDNS/support/-/wikis/False-Negative "About False Positive"
[hosts]: https://mypdns.org/mypdns/support/-/wikis/dns/DnsHosts "Hosts files a outdated blacklist format"
[issue]: https://mypdns.org/my-privacy-dns/matrix/-/issues "My Privacy DNS Domain records"
[mpdrf]: https://mypdns.org/my-privacy-dns/matrix/ "My Privacy DNS RPZ Firewall Filter"
[MR]: https://mypdns.org/my-privacy-dns/matrix/-/merge_requests "My Privacy DNS Merge Requests"
[Pi-hole]: https://mypdns.org/my-privacy-dns/matrix/-/blob/master/source/porn_filters/README.md#pi-hole "What is Pi-hole and it limitations"
[screenshot]: https://mypdns.org/MypDNS/support/-/wikis/Screenshot "What is a screenshot"


/health_status on_track

/label ~"NSFW::Strict" ~AdWare 

/publish

/weight 8

/severity low

/epic my-privacy-dns&2 

/milestone %"Google LLC." 

/iteration *iteration:14
